#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#define ROW 3
#define COL 3
//数组指针传参实现二维数组传参
void seek(int(*p)[COL],int r,int c,int key)
{
	int x = 0;
	int y = 0;
	for (x=0;x<r;x++)//打印一遍矩阵，即二维数组
	{
		for (y = 0; y < c; y++)
		{
			printf("%d ",*(*(p+x)+y));
		}
		printf("\n");
	}
	int i = 0;
	int j = c-1;
	while(i < r && j < c)
	{
		while(*(*(p + i) + j)< key)//跟每一行最后一个数比较，小于则下一行
		{
			i++;
		}
			for (j = c - 1; j >= 0; j--)//跟该行的列从高到低依次比较，等于key时打印退出
			{
				if (*(*(p + i) + j) == key)
				{
					printf("存在%d,在a[%d][%d]", *(*(p + i) + j), i, j);
					break;
				}
				else if(j==0)
				{
					printf("不存在%d", key);
					break;
				}
				else
				{
					;
				}
			}
			break;
	}
}
int main()
{
	int i = 0;
	int a[ROW][COL] = { 1,2,3,4,5,6,7,8,9 };
	printf("请输入要查找的数：");
	scanf("%d", &i);
	seek(a,ROW,COL,i);
	
	return 0;
}

//用构造结构体实现
//#include<stdio.h>
//#include<windows.h>
//
//#define ROW 3
//#define COL 3
//struct point
//{
//	int x;
//	int y;
//};//定义一个结构体返回坐标
//struct point find_key(int arr[ROW][COL], int key)
//{
//	int x = 0;
//	int y = COL - 1;
//	struct point ret = { -1,-1 };
//	while ((x <= ROW - 1) && (y >= 0))
//	{
//		if (key == arr[x][y])
//		{
//			ret.x = x;
//			ret.y = y;
//			return ret;
//		}
//		else if (key < arr[x][y])
//		{
//			y--;
//		}
//		else
//		{
//			x++;
//		}
//	}
//	return ret;
//}
//int main()
//{
//	int key = 0;
//	int arr[ROW][COL] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
//	printf("请输入要找的数:");
//	scanf_s("%d", &key);
//	struct point ret = find_key(arr, key);
//	if ((ret.x != -1) && (ret.y) != -1)
//	{
//		printf("找到了，坐标是%d ,%d\n", ret.x + 1, ret.y + 1);
//	}
//	else
//	{
//		printf("找不到\n");
//	}
//	system("pause");
//	return 0;
//}