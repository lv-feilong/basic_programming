#include<stdio.h>
//二分查找
int find_key(int a[],int i,int j,int key)
{
	int x = (i + j) / 2;
	while(j>i)
	{
		x = (i + j) / 2;
		if (a[x] == key)
		{
			return x;
		}
		else if (a[x] < key)
		{
				i = x+1;
		}
		else
		{
			j = x;
		}
	}
	if (i==j)
	{
		if (a[x] == key)
			return i;
		else
			return -1;
	}
}
int main()
{

	int arr[10] = {1,2,3,4,5,6,7,8,9,10};//数组必须有序
	int key = 0; 
	printf("输入需要查找的数：");
	scanf_s("%d", &key);
	
	//二分查找
	int i = 0;//左下标
	int j = 9;//右下标
	int k = 0;
	k = find_key(arr, i, j, key);
	if (k == -1)
		printf("没找到");
	else
		printf("下标是%d", k);
	return 0;
}