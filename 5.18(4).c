#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int gcd(int a, int b) {
	if (b == 0) return a;
	return gcd(b, a % b);
}

int lcm(int a, int b) {
	return a * b / gcd(a, b);
}

int main()
{
	int m, n, tmp, g, l;
	scanf("%d%d", &m, &n);
	if (m < n) {
		tmp = m;
		m = n;
		n = tmp;
	}
	g = gcd(m, n);
	l = lcm(m, n);
	printf("%d和%d的最大公因数为%d\n", m, n, g);
	printf("%d和%d的最小公倍数为%d", m, n, l);
	return 0;
}