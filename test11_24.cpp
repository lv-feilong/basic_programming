#include<stdio.h>
//判断x是否为素数，为素数返回0，非素数返回1
int isprime(int a)
{
	int i = 0;
	for (i = 2; i < a; i++)
	{
		if (a % i == 0)
		{
			//是合数，返回1
			a = 1;
			return a;
		}
	}
	//是素数，返回0
	a = 0;
	return a;
}
//求x的一个较小因数
int factor(int b)
{
	int y = 0;
	for (int i = 2; i < b; i++)
	{
		//如果有因数，赋值给y后，返回
		if (b % i == 0)
		{
			y = i;
			return y;
		}
	}
	//没有因数返回自身
	return b;
}

int main()
{
	int x;
	do {
		printf("请输入一个两位整数:");
		scanf("%d", &x);
	} while (x < 10 || x>100);
	//判断是否为素数
	int ret= isprime(x);
	//x是素数，为素数直接输出1×本身
	if (ret == 0)
	{
		printf("%d=1*%d\n", x,x);
	}
	//x非素数，分解因子
	else
	{
		int e[7];
		int count = 0;//计数，x因子个数
		e[count] = factor(x);//x的较小的因数赋值给e[0]
		e[count + 1] = x / e[count];//x的另一个较大因数赋值给e[1]
		
		//判断较大的因数是不是素数时，如果不是素数，需要再次分解
		while ((isprime(e[count+1]))==1)
		{
			count++;
			e[count + 1] = e[count];
			e[count]=factor(e[count]);//分解完后，较小因子的赋值给e[count]
			e[count + 1] = e[count + 1] / e[count];//分解完后，较大因子的赋值给e[count+1]，然后继续循环分解，直到所有的因子都成为素数
		}
		count++;//此时count的值为因子个数
		
		//循环打印
		printf("%d=%d", x, e[0]);
		for (int i = 1; i <= count; i++)
		{
			printf("*");
			printf("%d", e[i]);
		}
	}
	return 0;
}


