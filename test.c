#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#define MAXSIZE 100
typedef int ElemType;
typedef struct
{
	ElemType list[MAXSIZE];
	int last;
}SeqList;
void InitList(SeqList *L)
{
	L->last = -1;
}
void PutseqList(SeqList* L,int n)
{
	int i;
	for (i = 0; i < n; i++)
		scanf("%d", &(L->list[i]));
	L->last = L -> last + n;
}
int LengthList(SeqList* L)
{
	int Len;
	Len = L->last + 1;
	return Len;
}
int PositionList(SeqList* L, int X)
{
	int j;
	for (j = 0; j <= L->last; j++)
		if (X < L->list[j])
			return j + 1;
	return(L->last + 2);
}
int InsertList(SeqList* L,int i, int e)
{
	int k;
	if ((i < 1) || (i > L->last + 2))
	{
		printf("插入位置不合理");
		return(0);
	}
	if (L->last >= MAXSIZE - 1)
	{
		printf("表已满无法插入");
			return(0);
	}
	for (k = L->last; k >= i - 1; k--)
		L->list[k + 1] = L->list[k];
	L->list[i - 1] = e;
	L->last++;
	return(1);
}
int OutputSeqList(SeqList* L)
{
	int i;
	printf("输出结果为;");
	for (i = 0; i <= L->last; i++)
		printf("%d",L->list[i]);
	return(L->list[i]);
}
void main()
{
	int s, c;
	SeqList L;
	InitList(&L);
	printf("请输入顺序表长度:");
	scanf("%d", &s);
	printf("请输入递增顺序表:");
	PutseqList(&L, s);
	LengthList(&L);
	printf("表长为%d\n", LengthList(&L));
	printf("请输入要插入的元素:");
	scanf("%d", &c);
	InsertList(&L, PositionList(&L, c), c);
	OutputSeqList(&L);
	printf("\n");
}