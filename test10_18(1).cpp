#include<iostream>
using namespace std;
#include "Queue.h"

//struct ListNodeC
//{
//	int val;
//	struct ListNodeC* next;
//};
//
//struct ListNodeCPP
//{
//	int val;
//	ListNodeCPP* next;
//};
//
//int main()
//{
//	struct ListNodeC n1;
//	ListNodeCPP n2;
//	return 0;
//}

// C语言面向过程  --  数据和方法是分离的
// CPP面向对象    -- 数据和方法是封装在一起的
//struct Student
//{
//	void SetStudentInfo(const char* name, const char* gender, int age)
//	{
//		strcpy(_name, name);
//		strcpy(_gender, gender);
//		_age = age;
//	}
//
//	void PrintStudentInfo()
//	{
//		cout << _name << " " << _gender << " " << _age << endl;
//	}
//
//	char _name[20];
//	char _gender[3];
//	int _age;
//};
//
//// C
//namespace bitc
//{
//	struct Stack
//	{
//		int* a;
//		int top;
//		int capacity;
//	};
//
//	void StackInit(struct Stack* ps){}
//	void StackPush(struct Stack* ps, int x){}
//	// ...
//}
//
//// CPP
//namespace bitcpp
//{
//	struct Stack
//	{
//	// 访问限定符：
//	// 想让你在类外面直接访问的定义成共有
//	// 不想让你在类外面直接访问的定义成私有
//	public:
//		void Init(){}
//		void Push(int x){}
//		void Pop(){}
//		void Destory(){};
//		// ...
//
//	private:
//		int* a;
//		int top;
//		int capacity;
//	};
//}
//
//
//int main()
//{
//	Student s;
//	s.SetStudentInfo("Peter", "男", 18);
//	s.PrintStudentInfo();
//
//	struct bitc::Stack stc;
//	bitc::StackInit(&stc);
//	bitc::StackPush(&stc, 1);
//	bitc::StackPush(&stc, 2);
//
//	bitcpp::Stack stcpp;
//	stcpp.Init();
//	stcpp.Push(1);
//	stcpp.Push(2);
//	//stcpp.top = 0;
//
//	return 0;
//}

// CPP
//namespace bitcpp
//{
//	//struct Stack
//	class Stack
//	{
//		// 访问限定符：
//		// 想让你在类外面直接访问的定义成共有
//		// 不想让你在类外面直接访问的定义成私有
//	public:
//		void Init(){}
//		void Push(int x){}
//		void Pop(){}
//		void Destory(){};
//		// ...
//
//	private:
//		int* a;
//		int top;
//		int capacity;
//	};
//}
//
//
//int main()
//{
//	bitcpp::Stack stcpp;
//	stcpp.Init();
//	stcpp.Push(1);
//	stcpp.Push(2);
//
//	return 0;
//}

//int main()
//{
//	// 一个类可以实例化出多个对象
//	Queue q1;
//	q1.Init();
//	q1.Push(1);
//	q1.Push(2);
//	q1.Push(3);
//
//	Queue q2;
//	q2.Init();
//	q2.Push(10);
//
//	cout << sizeof(q1) << endl;
//	cout << sizeof(q2) << endl;
//
//	return 0;
//}

//class A
//{
//public:
//	void Print()
//	{}
//private:
//	int i;
//	char ch;
//};
//
//// 没有成员变量的类
//class B
//{
//public:
//	void Print()
//	{}
//};
//
//// 空类
//class C
//{};
//
//int main()
//{
//	cout << sizeof(A) << endl;
//	cout << sizeof(B) << endl;
//	cout << sizeof(C) << endl;
//
//	C c1, c2;
//	cout << &c1 << endl;
//	cout << &c2 << endl;
//
//
//	return 0;
//}


namespace bitc
{
	struct Stack
	{
		int* a;
		int top;
		int capacity;
	};

	void StackInit(struct Stack* ps){}
	void StackPush(struct Stack* ps, int x){}
	// ...
}


namespace bitcpp
{
	//struct Stack
	class Stack
	{
		// 访问限定符：
		// 想让你在类外面直接访问的定义成共有
		// 不想让你在类外面直接访问的定义成私有
	public:
		void Init(){}
		void Push(int x){}
		void Pop(){}
		void Destory(){};
		// ...

	private:
		int* a;
		int top;
		int capacity;
	};
}


class Date
{
public:
	// void Display(Date* this)
	void Display()
	{
		cout << _year << "-" << _month << "-" << _day << endl;
		cout << this->_year << "-" << this->_month << "-" << this->_day << endl;
	}

	
	// void Init(Date* this, int year, int month, int day) 
	void Init(int year, int month, int day)
	{
		// 成员函数里面。我们不加的话，默认会在成员前面this->
		// 我们也可以显示的在成员前面this->
		this->_year = year;
		this->_month = month;
		this->_day = day;
		
		// 对象可以调用成员函数，成员函数中还可以调用成员函数
		// 因为有this指针
		this->Display();
	}
private:
	int _year; // 年
	int _month; // 月
	int _day; // 日
};

// 1、this指针存在哪？  堆   栈  静态区  常量区


class A
{
public:
	void Show()
	{
		cout << "Show()" << endl;
	}

	void Print()
	{
		cout << _a << endl;
	}
private:
	int _a;
};

int main()
{
	//Date d1;
	//Date d2;
	//d1.Init(2021, 10, 8); // d1.Init(&d1, 2021, 10, 8);
	//d2.Init(2022, 10, 8); // d2.Init(&d2, 2021, 10, 8);
	//d1.Display(); // d1.Display(&d1);
	//d2.Display(); // d2.Display(&d2);

	// 下面程序的运行结果是什么? 编译不通过； 运行崩溃； 正常运行
/*	A* p = nullptr;
	p->Show();  */

	// 下面程序的运行结果是什么? 编译不通过； 运行崩溃； 正常运行
	A* p = nullptr;
	p->Print();

	return 0;
}