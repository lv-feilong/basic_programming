#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int my_sum1(int x)
{
	int i=1;
	int sum=0;
	while(x>sum)
	{
		sum += i;
		i++;
	}
	i--;
	return i;
}

int my_sum2(int q)
{
	int mysum2 = 0;
	for (int j = 0; j <= q; j++)
	{
		mysum2 += j;
	}
	return mysum2;
}

int my_sum(int L,int R)
{
	int l1 = my_sum1(L);//l1表示最左边数在第几轮
	printf("左边在第%d次循环\n",l1);
	int r1 = my_sum1(R);
	printf("右边在第%d次循环\n", r1);
	int y=r1-l1;
	int sum = 0;//总和
	int sum2 = 0;//左右间隔整轮数和
	int sum3 = 0;//
	int sum4 = 0;
	if (y >= 1)
	{
		for (int i = 0; i < y - 1; i++)
		{
			sum2 += my_sum2(l1 + i + 1);
		}
		int t=L-my_sum2(l1-1);
		printf("左边从%d开始\n", t);
		for (t; t<=l1; t++)
		{
			sum3+=t;
		}
		int p = R-my_sum2(r1-1);
		printf("右边在%d结束\n", p);
		for (int j = 0; j <= p; j++)
		{
			sum4 += j;
		}
		sum = sum2 + sum3 + sum4;
		return sum;
	}
	else
	{
		int t = L - my_sum2(l1 - 1);
		int p = R - my_sum2(r1 - 1);
		for (t; t <= p; t++)
		{
			sum += t;
		}
		return sum;
	}
}
int main()
{
	int T=0;
	int l = 0;
	int r = 0;
	scanf("%d", &T);//T表示查询次数
	for (int i = 0; i < T; i++)
	{
		scanf("%d %d",&l,&r);
		int a=my_sum(l, r);
		printf("%d\n", a);
	}

	return 0;
}