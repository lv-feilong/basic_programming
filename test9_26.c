//#include <iostream>
//using namespace std;  // C++标准库的东西
//
//int main()
//{
//	// 一定要注意这里跟C取地址用了一个符号 &
//	// 但是他们之前没有关联，各个各用处
//	int a = 10;
//	int& b = a;
//	int& c = a;
//	int& d = b;
//
//	c = 20;
//	d = 30;
//
//	int& e;
//
//	return 0;
//}

//#include <iostream>
//using namespace std;  // C++标准库的东西
//
//int main()
//{
//	int a = 10;
//	int& b = a;
//
//	int c = 20;
//	b = c;
//
//	return 0;
//}


//#include <iostream>
//using namespace std;  // C++标准库的东西
//
//void Swap(int* p1, int* p2)
//{
//	int tmp = *p1;
//	*p1 = *p2;
//	*p2 = tmp;
//}
//
//void Swap(int& rx, int& ry)
//{
//	int tmp = rx;
//	rx = ry;
//	ry = tmp;
//}
//
//typedef struct Stack
//{
//	int* a;
//	int top;
//	int capacity;
//}ST;
//
//void StackInit(ST* ps)
//{
//	ps->a = NULL;
//	ps->top = ps->capacity = 0;
//}
//
//void StackInit(ST& rs)
//{
//	rs.a = NULL;
//	rs.top = rs.capacity = 0;
//}
//
//typedef struct SListNode
//{
//	struct SListNode* next;
//	int val;
//}SLTNode, *PSLTNode;
//
//// C
//void SListPushBack(SLTNode** pphead, int x)
//{
//	SLTNode* newnode;
//	if (*pphead == NULL)
//	{
//		*pphead = newnode;
//	}
//	else
//	{
//
//	}
//}
//
//// CPP
//void SListPushBack(PSLTNode& phead, int x)
////void SListPushBack(SLTNode*& phead, int x)
//{
//	SLTNode* newnode;
//	if (phead == NULL)
//	{
//		phead = newnode;
//	}
//	else
//	{
//
//	}
//}
//
//
//int main()
//{
//	int x = 0, y = 1;
//	Swap(&x, &y);
//	Swap(x, y);
//
//	ST st;
//	//StackInit(&st);
//	StackInit(st);
//
//	int* p1 = &x;
//	int* p2 = &y;
//
//	int*& p3 = p1;  // 指针变量取别名
//	*p3 = 10;
//	p3 = p2;
//
//	SListNode* plist = NULL;
//	//SListPushBack(&plist, 1);
//	//SListPushBack(&plist, 2);
//
//	SListPushBack(plist, 1);
//	SListPushBack(plist, 2);
//
//
//	return 0;
//}


#include<iostream>
using namespace std;

//int main()
//{
//	// 我变成你的别名的条件：不变或者缩小的你的读写权限是可以的，放大你的读写权限不行的
//	const int a = 10;
//	// int& b = a;  // 变成你的别名，还能修改你  (不行)
//
//	int c = 20;
//	const int& d = c; // 变成你的别名，不能修改你  (行)
//
//	return 0;
//}

//typedef struct Stack
//{
//	int a[1000];
//	int top;
//	int capacity;
//}ST;
//
//void StackInit(ST& s) // 这里传引用是为了形参的改变，影响实参
//{
//	s.top = 0;
//	s.capacity = 1000;
//	// ...
//}
//
//void PrintStack(const ST& s)  
//// 1、传引用是为了减少传值传参时的拷贝  
//// 2、可以保护形参形参不会被改变
//{
//	// 假设有这样一个逻辑 -> 如果时const引用，这里就可以被检查出来
//	//if (s.capacity = 0)  // 本来应该是== ，不小心写成=
//	{
//	}
//}
//
//void func(const int& n) // const引用做参数的第二个好处，即可接收变量，也可以接收常量
//{
//
//}
//
//// 总结：函数传参如果想减少拷贝用了引用传参，如果函数中不改变这个参数
//// 最好用const 引用传参
//int main()
//{
//	ST st;
//	StackInit(st);
//	// ...
//	PrintStack(st);
//
//	int i = 10;
//	const int j = 30;
//	func(i);
//	func(20);
//	func(j);
//
//
//	return 0;
//}

//int main()
//{
//	int i = 10;
//	double d = i;
//	const double& r = i;
//
//	char ch = 0xff;
//	int j = 0xff;
//	int k = ch;
//	// ch就会提升转换成int类型，再比较，提升时部位符号位
//	// 这里把ch提升，你把ch变成一个int吗？，是生成一个临时变量int
//	if (ch == j) 
//	{
//		cout << "相同" << endl;
//	}
//	else
//	{
//		cout << "不相同" << endl;
//	}
//
//	return 0;
//}

// 引用还可以做返回值
//int Add(int a, int b)
//{
//	int c = a + b;
//	return c;
//}
//
//int main()
//{
//	const int& ret = Add(1, 2);
//	Add(3, 4);
//	cout << "Add(1, 2) is :" << ret << endl;
//	return 0;
//}

int& Add(int a, int b)
{
	int c = a + b;
	return c;
}

int main()
{
	int& ret = Add(1, 2);
	Add(3, 4);
	cout << "Add(1, 2) is :" << ret << endl;
	return 0;
}
