#include<iostream>
using namespace std;
//运算符重载
class Date
{
public:
	Date(int year = 0, int mon = 1, int day = 1)
	{
		_year = year;
		_mon = mon;
		_day = day;
	}
	//小于号重载
	bool operator<(const Date& x)
	{
		if (this->_year < x._year)
		{
			return true;
		}
		else if (this->_year == x._year && this->_mon < x._mon)
		{
			return true;
		}
		else if(this->_year == x._year&&this->_mon == x._mon && this->_day < x._day)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	//赋值运算符重载
	Date& operator=(const Date& x)
	{
		if (this != &x)
		{
			_year=x._year;
			_mon = x._mon;
			_day = x._day;
		}
		return *this;
	}
	//打印重载
	void Print()
	{
		cout<<_year<<"/"<<_mon<<"/"<<_day<<endl;
	}
private:
	int _year;
	int _mon;
	int _day;
};
int main()
{
	Date d1(2021,10,11);
	Date d2(2020,11,11);
	Date d3(2021,11,11);
	//d2 < d1;//d2.operator(&d1)
	cout << (d2 < d1) << endl;
}