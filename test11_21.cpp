#include<stdio.h>
#include<math.h>
int my_pow(int x, int y)
{
    long i, sum = 1;
    int count = 0;
    for (i = y; i > 0; i--)
    {
        sum = (sum * x) % 998244353;
        count++;
    }
    printf("sum=%d\n", sum);
    printf("count=%d", count);
    return sum;
}
int main()
{
    int T = 0;
    scanf_s("%d", &T);
    int n = 0, i = 0;
    for (i = 0; i < T; i++)
    {
        scanf_s("%d", &n);
        int a = 2;
        for (i = 0; i < n / 2 - 1; i++)
        {
            a += (2 * my_pow(2, i + 1));
        }
        printf("%d\n", a% 998244353);
    }
    return 0;
}