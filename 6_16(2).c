#include<stdio.h>
struct S
{
    int a;
    char b;
    char c[20];
    double d;
};

struct stu     //struct结构体关键字  stu结构体标签   struct stu结构体类型
{
    //成员变量，可以是数值，指针，甚至是其他结构体

    char name[10];//名字
    int class;//年级
    int grade;//成绩
    int rank;//排名 
    struct S s;
}s1, s2, s3;         //结构体变量 ，全局变量

void print1(struct stu s)
{
    printf("%s\n", s.name);
    printf("%d\n", s.grade);
   // printf("%s\n", s.s.b);
}

void print2(struct stu* p)
{
    printf("%s\n", p->name);
    printf("%d\n", p->grade);
    //printf("%s\n", p->s.b);
}
int main()
{
    //初始化的时候，成员如果是结构体，需要用{ }括起来。
    struct stu s4 = { "张三",5,100,1,{100,"a","hello world",1.11} };
    print1(s4);
    print2(&s4);
    return 0;
}