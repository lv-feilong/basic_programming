#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int gcd(int a, int b)//辗转相除 
{
	if (b == 0) return a;
	return gcd(b, a % b);
}

int main()
{
	int m, n, tmp, g;
	scanf("%d%d", &m, &n);
	if (m < n) {
		tmp = m;
		m = n;
		n = tmp;
	}
	g = gcd(m, n);
	printf("%d和%d的最大公因数为%d", m, n, g);
	return 0;
}