#include<stdio.h>
void f1(int i)
{
	printf("f(int)\n");
}
void f2(int* i)
{
	printf("f(int*)\n");
}
int main()
{
	int* p1 = 0;
	int* p2 = NULL;
	f1(0);
	f2(NULL);
	return 0;
}