#include<iostream>
using namespace std;

//int& Add(int a, int b)
//{
//	int c = a + b;
//	return c;
//}
//
//int main()
//{
//	int& ret = Add(1, 2);
//	//Add(3, 4);
//	printf("hello world\n");
//	cout << "Add(1, 2) is :" << ret << endl;
//	return 0;
//}


//int& Count()
//{
//	static int n = 0;
//	n++;
//	// ...
//	return n;
//}
//
//int main()
//{
//	return 0;
//}

#include <time.h>
struct A
{
	int a[10000];
};

void TestFunc1(A a) {}

void TestFunc2(A& a) {}

void TestRefAndValue()
{
	A a;
	// 以值作为函数参数
	size_t begin1 = clock();
	for (size_t i = 0; i < 10000; ++i)
		TestFunc1(a);
	size_t end1 = clock();

	// 以引用作为函数参数
	size_t begin2 = clock();
	for (size_t i = 0; i < 10000; ++i)
		TestFunc2(a);
	size_t end2 = clock();

	// 分别计算两个函数运行结束后的时间
	cout << "TestFunc1(A)-time:" << end1 - begin1 << endl;
	cout << "TestFunc2(A&)-time:" << end2 - begin2 << endl;
}

A a;
// 值返回
A TestFunc1() { return a; }
// 引用返回
A& TestFunc2() { return a; }

void TestReturnByRefOrValue()
{
	// 以值作为函数的返回值类型
	size_t begin1 = clock();
	for (size_t i = 0; i < 10000; ++i)
		TestFunc1();
	size_t end1 = clock();

	// 以引用作为函数的返回值类型
	size_t begin2 = clock();
	for (size_t i = 0; i < 10000; ++i)
		TestFunc2();
	size_t end2 = clock();

	// 计算两个函数运算完成之后的时间
	cout << "TestFunc1 time:" << end1 - begin1 << endl;
	cout << "TestFunc2 time:" << end2 - begin2 << endl;
}

//int main()
//{
//	TestRefAndValue();
//	TestReturnByRefOrValue();
//
//	return 0;
//}

//int main()
//{
//	int a = 10;
//
//	int& ra = a;
//	ra = 20;
//
//	int* pa = &a;
//	*pa = 30;
//
//	return 0;
//}

// 下面的写法啊就是大家常见的一些错误
//#define ADD(int a, int b) return (a + b);
//#define ADD(a, b) (a + b);
//#define ADD(a, b) (a + b)
#define ADD(a, b) ((a) + (b))

// 调用的地方展开，没有建立栈帧的开销
// inline debug默认是不会展开的，支持调试
// release或者debug设置一下优化等级才会展开
inline int add(int x, int y)
{
	return x + y;
}

//int main()
//{
//	// 写宏就要想清楚，你的宏被替换以后对不对？
//	//cout << return（1+2）; << endl;
//	//cout << (1 + 2); << endl;
//	//cout << ADD(1, 2) << endl;
//	//cout << ADD(1, 2)*10 << endl;
//
//	//int x = 10, y = 20;
//	////cout << (x|y + x&y)<< endl;
//	//cout << ADD(x|y, x&y)<< endl;
//
//	int ret = add(10, 20);
//	cout << ret << endl;
//
//	return 0;
//}

#include <map>
#include <string>

//int main()
//{
//	int a = 1;
//	char b = 'a';
//
//	// 通过右边赋值对象，自动推导变量类型
//	auto c = a;  
//	auto d = b;
//
//	// 可以去看变量的实际类型
//	cout << typeid(c).name() << endl;
//	cout << typeid(d).name() << endl;
//
//
//	map<string, string> dict;
//	//map<string, string>::iterator it = dict.begin();
//	// 类型太复杂，太长，auto自动推导简化代码
//	// 缺点：一定程度牺牲了代码的可读性
//	auto it = dict.begin();
//	cout << typeid(it).name() << endl;
//	return 0;
//}

//
//int main()
//{
//	int x = 10;
//	auto a = &x;
//	auto* b = &x;
//	auto& c = x;
//
//	cout << typeid(a).name() << endl;
//	cout << typeid(b).name() << endl;
//	cout << typeid(c).name() << endl;
//}

//void TestArrayFor(int array[])
//{
//	// 不行 -- array已经不是数组
//	for (auto& e : array)
//		cout << e << endl;
//}

void TestFor()
{
	int array[] = { 1, 2, 3, 4, 5 };

	//TestArrayFor(array);

	// C / C++98
	for (int i = 0; i < sizeof(array) / sizeof(array[0]); ++i)
		array[i] *= 2;

	for (int* p = array; p < array + sizeof(array) / sizeof(array[0]); ++p)
		cout << *p << " ";
	cout << endl;

	// C++11 提供一种新的访问数组的方式，范围for
	// 自动依次取数组中的值赋值给e，自动判断结束
	//for (auto e : array)
	for (auto& e : array)
	{
		e *= 2;
	}

	for (auto e : array)
	{
		cout << e << " ";
	}
	cout << endl;

	// 下面的代码看看即可
	//map<string, string> dict = { { "string", "字符串" }, { "sort", "排序" }, { "true", "真" } };
	//// 迭代器遍历
	//map<string, string>::iterator it = dict.begin();
	//while (it != dict.end())
	//{
	//	cout << it->first << ":" << it->second << endl;
	//	++it;
	//}
	//cout << endl;

	//// C++11范围for还用更简单的方便遍历容器
	//// 这里不需要大家看懂，这里要学了类和对象、泛型、STL才能懂
	//// 这里知识让大家感受，知道范围for他使用来简化代码
	//for (auto e : dict)
	//{
	//	cout << e.first << ":" << e.second << endl;
	//}
	//cout << endl;
}

//int main()
//{
//	TestFor();
//
//	return 0;
//}

void f(int)
{
	cout << "f(int)" << endl;
}

void f(int*)
{
	cout << "f(int*)" << endl;
}

int main()
{
	// C++98
	int* p1 = NULL;
	int* p2 = 0;
	f(0);
	f(NULL);

	// C++11 以后推荐用它当空指针使用
	int* p3 = nullptr;
	f(0);
	f(nullptr);

	return 0;
}
