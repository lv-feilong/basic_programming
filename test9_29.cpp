/#include<stdio.h>
//
//// C++兼容C绝大多数的语法
//int main()
//{
//	printf("hello world\n");
//
//	return 0;
//}

//#include<iostream>
//using namespace std;
//
//int main()
//{
//	cout << "hello world" << endl;
//
//	return 0;
//}

#include <stdio.h>
#include <stdlib.h>
#include "Queue.h"
#include "Stack.h"

//int a = 0;
//// 我们跟库的命名冲突了
//// 实际大型项目开发，还存在同事之间定义的变量/函数/类型命名冲突等等
//// C++ 提出命名空间来解决名字冲突的问题
//
//namespace bit // 定义个命名空间域
//{
//	// 定义变量/函数/类型
//	int rand = 10;
//
//	int Add(int left, int right)
//	{
//		return left + right;
//	}
//
//	struct Node
//	{
//		struct Node* next;
//		int val;
//	};
//
//	// 命名空间可以嵌套定义
//	namespace dy
//	{
//		int c;
//		int d;
//		int Sub(int left, int right)
//		{
//			return left - right;
//		}
//	}
//}
//
//namespace cpp
//{
//	int rand = 100;
//}
//
//int main()
//{
//	int a = 1;
//	printf("%p\n", rand);
//	printf("%d\n", bit::rand);
//	printf("%d\n", cpp::rand);
//
//	bit::Add(1, 2);
//
//	struct bit::Node node1;
//
//	bit::dy::Sub(1, 2);
//
//	struct bit::Stack st;
//
//	return 0;
//}

//namespace bit // 定义个命名空间域
//{
//	// 定义变量/函数/类型
//	int rand = 10;
//
//	int Add(int left, int right)
//	{
//		return left + right;
//	}
//
//	typedef struct Node
//	{
//		struct Node* next;
//		int val;
//	}ListNode;
//}
//
//// 如何使用命名空间里面的东西？
//// 三种方式
//// 1、全部直接展开到全局. 优点：用起来方便。 缺点：把自己的定义暴露出去了，导致命名污染
//// using namespace bit;
////using namespace std;  // std是包含C++标准库的命名空间
//
//// 2、访问每个命名空间中的东西时，指定命名空间
//// 优点：不存在命名污染。 缺点：用起来麻烦，每个都的去指定命名空间
//// std::rand
//
//// 3、把某个展开  -- 把常用给展开
//// 折衷1和2优解决方案
//using bit::Node;
//using bit::Add;
//
//int main()
//{
//	//Add(1, 2);
//	//struct Stack st;
//	//printf("%d", rand);
//
//	struct Node n1;
//	Add(1, 2);
//	bit::rand = 100;
//
//	bit::ListNode n2;
//
//	return 0;
//}

//#include <iostream>
//#include <list>
//#include <string>
//
//// 工程项目中常见得对命名空间得用法
//// 展开常用
//using std::cout;
//using std::endl;
//
//int main()
//{
//	cout << "hello" << endl;
//	std::string s;
//	std::list<int> lt;
//
//	return 0;
//}

//#include <iostream>
//#include <list>
//#include <string>
//
//// 日常练习 -- 不需要像项目中那么规范
//using namespace std;
//
//int main()
//{
//	cout << "hello" << endl;
//	string s;
//
//	return 0;
//}


//#include <iostream>
//// 日常练习 -- 不需要像项目中那么规范
//using namespace std;
//
//int main()
//{
//	int n;
//	cin >> n;  // >>输入运算符/流提取运算符
//
//	/*int* a = (int*)malloc(sizeof(int)*n);
//	for (int i = 0; i < n; ++i)
//	{
//	cin >> a[i];
//	}*/
//
//	double* a = (double*)malloc(sizeof(double)*n);
//	for (int i = 0; i < n; ++i)
//	{
//		cin >> a[i];
//	}
//
//	for (int i = 0; i < n; ++i)
//	{
//		cout << a[i]<<" "; // <<输出运算符/流插入运算符
//	}
//	cout << endl;
//	cout << '\n';
//
//
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//struct Student
//{
//	char name[10];
//	int age;
//};

//int main()
//{
//	// 自动识别类型
//	char ch = 'A';
//	int i = 10;
//	int* p = &i;
//	double d = 1.111111;
//
//	// 自动识别变量得类型
//	cout << ch << endl;
//	cout << i << endl;
//	cout << p << endl;
//	cout << d << endl;
//
//	// 类似下面得场景用printf更好用一些
//	// 建议C++中，不用纠结到底要用哪个，哪个好用用哪个
//	struct Student s = { "张三", 18 };
//	cout << "名字:" << s.name << " " << "年龄:" << s.age << endl;
//	printf("名字:%s 年龄:%d\n", s.name, s.age);
//
//	printf("%.2f\n", d);
//
//	return 0;
//}

#include<iostream>
using namespace std;
using namespace bit;

//void TestFunc(int a = 0) // 参数缺省值
//{
//	cout << a << endl;
//}
//
//
//int main()
//{
//	TestFunc(); // 不传a就用缺省的 - 等价于 TestFunc(0)；
//	TestFunc(10); // 传了就没缺省参数什么用了
//
//	// 假设我明确知道这里至少要存100个数据到st1里面去
//	struct Stack st1; 
//	StackInit(&st1, 100);
//
//	// 假设我不知道st2会存多少个数据
//	struct Stack st2;
//	StackInit(&st1);
//
//	return 0;
//}

//// 全缺省
//void TestFunc(int a = 10, int b = 20, int c = 30)
//{
//	cout << "a = " << a << endl;
//	cout << "b = " << b << endl;
//	cout << "c = " << c << endl;
//	cout << endl;
//}
//
//int main()
//{
//	TestFunc();
//	TestFunc(1);
//	TestFunc(1,2);
//	TestFunc(1, 2, 3);
//
//	return 0;
//}

// 半缺省（部分缺省）
// 半缺省参数必须从右往左依次来给出，不能间隔着给
//void TestFunc(int a, int b = 10, int c = 20)
//{
//	cout << "a = " << a << endl;
//	cout << "b = " << b << endl;
//	cout << "c = " << c << endl<<endl;
//}
//
//int main()
//{
//	TestFunc(1);
//	TestFunc(1, 2);
//	TestFunc(1, 2, 3);
//
//	return 0;
//}

// 函数重载
int Add(int left, int right)
{
	return left + right;
}

double Add(double left, double right)
{
	return left + right;
}

long Add(long left, long right)
{
	return left + right;
}

void Swap(int* p1, int* p2)
{}

void Swap(double* p1, double* p2)
{}

int main()
{
	Add(10, 20);
	Add(10.0, 20.0);
	Add(10L, 20L);

	return 0;
}
