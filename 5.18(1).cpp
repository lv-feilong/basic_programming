#include<stdio.h>
#include<string.h>
void funtion(char*dest,char*end)
{
	while (dest<end)
	{
		char tump = ' ';
		tump = *dest;
		*dest++ = *end;
		*end-- = tump;
	}
}
int main()
{
	char arr[1000] = { 0 };
	//三步法倒序
	//1.输入
	gets_s(arr);
	//2.将字符串整体反转
	int len = strlen(arr);
	funtion(arr, arr + len - 1);
	//3.将单词逐个逆序
	char* start = arr;
	while (*start)
	{
		char* end=start;
		while (*end != '\0' && *end != ' ')
		{
			end++;
		}
		//单个字母逆序
		funtion(start,end-1);
		if (*end==' ')
		{
			start = end+1;
		}
		else
		{
			start = end;
		}	
	}
	printf("%s\n", arr);
	return 0;
}